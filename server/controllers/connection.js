// This is the connection to the Elasticsearch database.

// First import the client library
const {Client} = require('@elastic/elasticsearch');

// Now grab the username, password, host and port from the environment
const esHost = process.env.DC_API_ES_HOST || 'localhost';
const esPort = process.env.DC_API_ES_PORT || 9200;
const esUsername = process.env.DC_API_ES_USERNAME;
const esPassword = process.env.DC_API_ES_PASSWORD;

// Grab the logging level
const logLevel = process.env.DC_API_LOG_LEVEL || 'info';

// Create a logger
var winston = require('winston');
const logger = winston.createLogger({
    level: logLevel,
    transports: [
        new winston.transports.Console()
    ]
});

// Now create the client connection
const client = new Client({node: 'http://' + esUsername + ':' + esPassword + '@' + esHost + ':' + esPort});
logger.info("Will connect to the elasticsearch server at " + esHost);

// TODO kgomes, put something in here that pauses the entire application until a connection is made and keep trying

// Check cluster health
client.cluster.health({},function(err,resp,status) {
    logger.info("-- Client Health --",resp);
});

// Export the connection
module.exports = client;
