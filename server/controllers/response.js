// This represents a Response object to help with formatting responses from the API
function Response() {

    // The properties of the response
    this.code;
    this.apiVersion;
    this.data = {
        kind: '',
        fields: [],
        itemsPerPage: 0,
        startIndex: 0,
        nextLink: '',
        previousLink: '',
        selfLink: '',
        items: []
    };

    // Methods to get and set the properties
    this.setCode = function (code) {
        this.code = code;
    };
    this.setAPIVersion = function (apiVersion) {
        this.apiVersion = apiVersion;
    };
    this.setDataKind = function (kind) {
        this.data.kind = kind;
    };
    this.addDataField = function (field) {
        this.data.fields.push(field);
    };
    this.clearDataFields = function () {
        this.data.fields = [];
    };
    this.setDataItemsPerPage = function (itemsPerPage) {
        this.data.itemsPerPage = itemsPerPage;
    };
    this.setDataStartIndex = function (startIndex) {
        this.data.startIndex = startIndex;
    };
    this.setDataNextLink = function (nextLink) {
        this.data.nextLink = nextLink;
    };
    this.setDataPreviousLink = function (previousLink) {
        this.data.previousLink = previousLink;
    };
    this.setDataSelfLink = function (selfLink) {
        this.data.selfLink = selfLink;
    };
    this.addDataItem = function (item) {
        // Grab all the property keys and add them to the fields
        for (var key in item) {
            // Make sure it's not a function
            if ({}.toString.call(item[key]) === '[object Function]') {
                // Do nothing
            } else {
                // See if the field is in the array of items already
                if (!this.data.fields.includes(key)) {
                    this.addDataField(key);
                }
            }
        }
        this.data.items.push(item);
    }
}

module.exports = Response;
