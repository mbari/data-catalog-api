// TODO kgomes, this file should be replaced by the JavaScript protocol buffers generated from the mbari-data-model
// project. https://bitbucket.org/mbari/mbari-data-model

// This is a function to help represent a Resource as an object in JavaScript
function Resource() {

    // The properties of the resource
    this.repo;
    this.uuid;
    this.content;
    this.data;
    this.file;
    this.processing_messages;
    this.locators;
    this.metadata;
    this.path;

    // A utility function to convert
    this.convertDateFieldToISOString = function (dateValue) {
        // Create the date to be returned
        var dateToReturn;

        // First check to see if it's a number or a string
        if (typeof dateValue === 'string') {
            // Even though it's a string, it could be a string representation of epoch seconds or millis, try to convert
            var epoch = null;
            try {
                epoch = Number.parseInt(dateValue);
            } catch (e) {
                console.log("Could not convert dateValue to number");
                console.log(dateValue);
                console.log(e);
            }

            // If it was successfully converted
            if (epoch) {
                // Create a date at epoch 0
                dateToReturn = new Date(0);
                // If the number is greater than 95617584000, it's clearly in millis since this would be the year 5000
                // and I don't think this software will be around that long
                if (epoch > 95617584000) {
                    dateToReturn.setUTCMilliseconds(epoch);
                } else {
                    dateToReturn.setUTCSeconds(epoch);
                }
            } else {
                // If it's a string, it could be a string representation, try to just create a Date object
                try {
                    dateToReturn = new Date(dateValue);
                } catch (e) {
                    // TODO kgomes log warning!
                    console.log("Error trying to convert date value to Date");
                    console.log(dateValue);
                    console.log(e);
                }
            }

        } else {
            // If it's a number
            if (typeof dateValue === 'number') {
                dateToReturn = new Date(0);
                // If the number is greater than 95617584000, it's clearly in millis since this would be the year 5000
                // and I don't think this software will be around that long
                if (dateValue > 95617584000) {
                    dateToReturn.setUTCMilliseconds(epoch);
                } else {
                    dateToReturn.setUTCSeconds(epoch);
                }
            } else {
                // Maybe it's already a date object
                if (dateValue instanceof Date) {
                    dateToReturn = dateValue;
                } else {
                    console.log("Could not figure out what incoming dateValue was, not converting it");
                    console.log(dateValue);
                }
            }
        }

        // Make sure and object was created
        if (dateToReturn) {
            return dateToReturn.toISOString();
        } else {
            return;
        }
    };

    // A method to set the repo
    this.setRepo = function(repo) {
        this.repo = repo;
    };

    // This is a method that will set the variables given a JSON object with resource properties
    this.setFromSource = function (source) {
        // Set the repo
        if (source['repo']) {
            this.repo = source['repo'];
        }

        // Set the UUID
        if (source['uuid']) {
            this.uuid = source['uuid'];
        }

        // The content
        if (source['content']) {
            this.content = source['content'];
        }

        // The data
        if (source.data) {
            this.data = source.data;
        }
        // Set the file properties
        if (source.file) {
            this.file = source.file;

            // Now convert any dates from epoch millis
            if (this.file.created) this.file.created =
                this.convertDateFieldToISOString(this.file.created);

            // Convert file size to number
            try {
                if (this.file.file_size) this.file.file_size = Number.parseInt(this.file.file_size);
            } catch (e) {
                console.log("Failed to convert file size " + this.file.file_size + " to a number");
                console.log(e);
            }

            // Last accessed
            if (this.file.last_accessed) this.file.last_accessed =
                this.convertDateFieldToISOString(this.file.last_accessed);

            // Last modified
            if (this.file.last_modified) this.file.last_modified =
                this.convertDateFieldToISOString(this.file.last_modified);

        }

        // Set locators
        if (source.locators) {
            this.locators = source.locators;
        }

        // Set the metadata
        if (source.metadata) {
            this.metadata = source.metadata;
        }

        // Set the path
        if (source.path) {
            this.path = source.path;
        }

        // Set any processing messages
        if (source.processing_messages) {
            this.processing_messages = source.processing_messages;
        }
    }
}

module.exports = Resource;
