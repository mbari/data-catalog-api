// This represents an Error object to help with formatting responses from the API
function Error() {

    // The properties of the response
    this.code;
    this.message = '';
    this.errors = [];

    // Methods to get and set the properties
    this.getCode = function () {
        return this.code;
    }
    this.setCode = function (code) {
        this.code = code;
    }

    this.getMessage = function () {
        return this.message;
    }
    this.setMessage = function (message) {
        this.message = message;
    }

    // A method to add an error message to the array of errors
    this.addError = function (errorCode, domain, reason, message, extendedHelp, sendReport, developerMessage) {
        // Create an error object with defaults
        var error = {
            errorCode: -1,
            domain: '',
            reason: '',
            message: '',
            extendedHelp: '',
            sendReport: '',
            developerMessage: ''
        };

        // Add properties if they exist
        if (errorCode) error['errorCode'] = errorCode;
        if (domain) error['domain'] = domain;
        if (reason) error['reason'] = reason;
        if (message) error['message'] = message;
        if (extendedHelp) error['extendedHelp'] = extendedHelp;
        if (sendReport) error['sendReport'] = sendReport;
        if (developerMessage) error['developerMessage'] = developerMessage;

        // Add it to the array
        this.errors.push(error);
    }
}

module.exports = Error;
