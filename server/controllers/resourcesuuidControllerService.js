'use strict'

// This is the method to return a Resource with a matching UUID
module.exports.getResourceByUUID = function getResourceByUUID(req, res, next) {

    // Make sure the repo is specified
    if (req && req['repo'] && req['repo'].value) {
        // Grab the repo name
        var repo = req['repo'].value;
        logger.debug("Will search for document in repo " + repo);

        // Make sure the UUID is in the request
        if (req && req['uuid'] && req['uuid'].value) {

            // Grab the UUID from the request
            var uuidToFind = req['uuid'].value;
            logger.debug("getResourceByUUID called with UUID: " + uuidToFind);

            // Set some default fields to return and then check to see if the caller specified what fields
            // they want returned
            var fieldsToReturn = ["uuid", "file", "locators", "path"];
            if (req['fieldsToReturn'] && req['fieldsToReturn'].value) {
                // Split it into an array using commas
                fieldsToReturn = req['fieldsToReturn'].value.split(',');
            }

            // Let's search for the doc with the given UUID
            client.search({
                index: repo,
                type: 'resource',
                body: {
                    _source: fieldsToReturn,
                    query: {
                        match: {
                            "uuid": uuidToFind
                        }
                    },
                }
            }, function (error, response, status) {
                // Check for any error
                if (error) {
                    // This is some kind of server side error that was not expected
                    logger.error("Unknown error trying to find Resource with UUID " + uuidToFind);
                    logger.error(error.message);

                    // Respond with an error
                    var errorToReturn = new Error();
                    errorToReturn.setCode(500);
                    errorToReturn.setMessage("There was an unexpected server error: " + error.message);

                    // Send the error
                    res.status(500).send(errorToReturn);
                } else {
                    // First check to see if anything was found at all
                    if (response && response.body && response.body.hits && response.body.hits.hits &&
                        response.body.hits.hits.length > 0) {
                        // Let's grab the first hit which should be the only object
                        var resourceHit = response.body.hits.hits[0];

                        // Create a the Resource to return
                        var resourceToReturn = new Resource();
                        resourceToReturn.setFromSource(resourceHit['_source']);
                        logger.debug("Resource with UUID " + uuidToFind + " is: ");
                        logger.debug(resourceToReturn);

                        // Make sure the repo is set on the resource
                        resourceToReturn.setRepo(repo);

                        // Create a response object
                        var responseToSend = new Response();
                        responseToSend.setCode(200);
                        responseToSend.setAPIVersion(process.env.DC_API_VERSION);
                        responseToSend.setDataKind('resources');
                        responseToSend.setDataItemsPerPage(1);
                        responseToSend.addDataItem(resourceToReturn);

                        // Send it
                        res.status(200).send(responseToSend);
                    } else {
                        // Nothing was found, return a 404
                        var errorToReturn = new Error();
                        errorToReturn.setCode(404);
                        errorToReturn.setMessage("No object with UUID " + uuidToFind + " found.");

                        // Send it
                        res.status(404).send(errorToReturn);
                    }
                }
            });
        } else {
            // There was no UUID in the request, create an Error to return
            var errorToReturn = new Error();
            errorToReturn.setCode(400);
            errorToReturn.setMessage("Invalid UUID supplied");

            // Send it
            res.status(400).json(errorToReturn);
        }
    } else {
        // There was no repo in the request, create an Error to return
        var errorToReturn = new Error();
        errorToReturn.setCode(400);
        errorToReturn.setMessage("Invalid repo supplied");

        // Send it
        res.status(400).json(errorToReturn);
    }
};

// Grab the logging library and create a logger
var winston = require('winston');
const logger = winston.createLogger({
    level: 'debug',
    transports: [
        new winston.transports.Console()
    ]
});

// Grab the connection to Elasticsearch
var client = require('./connection');

// Grab the Resource definition
var Resource = require('./resource');

// And the Response and Error defintions too
var Response = require('./response');
var Error = require('./error');
