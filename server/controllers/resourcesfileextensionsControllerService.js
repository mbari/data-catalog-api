'use strict'

module.exports.getResourceFileExtensions = function getResourceFileExtensions(req, res, next) {

    // Make sure the repo was specified
    if (req && req['repo'] && req['repo'].value) {
        // Grab the repo name
        var repo = req['repo'].value;
        logger.debug("Will search in repo " + repo);

        // Search for content type distinct list
        client.search({
            index: repo,
            type: 'resource',
            body: {
                "size": 0,
                "aggs": {
                    "file-extension-list": {
                        "terms": {
                            "field": "file.extension",
                            "size": 1000000,
                            "order": {
                                "_key": "asc"
                            }
                        }
                    }
                }
            }
        }, function (error, response, status) {
            if (error) {
                // This is some kind of server side error that was not expected
                logger.error("Unknown error trying to get list of file extensions");
                logger.error(error.message);

                // Respond with an error
                var errorToReturn = new Error();
                errorToReturn.setCode(500);
                errorToReturn.setMessage("There was an unexpected server error trying to get " +
                    "list of file extension: " + error.message);

                // Send the error
                res.status(500).send(errorToReturn);
            } else {
                // Make sure some results were returned
                if (response && response.body && response.body.aggregations &&
                    response.body.aggregations['file-extension-list'] &&
                    response.body.aggregations['file-extension-list'].buckets &&
                    response.body.aggregations['file-extension-list'].buckets.length > 0) {

                    // Create a response object
                    var responseToSend = new Response();
                    responseToSend.setCode(200);
                    responseToSend.setAPIVersion(process.env.DC_API_VERSION);
                    responseToSend.setDataKind('string');
                    responseToSend.setDataSelfLink(process.env.DC_API_BASE_URL + '/resources/file-extensions?repo=' + repo);

                    // Iterate over the results
                    for (var i = 0; i < response.body.aggregations['file-extension-list'].buckets.length; i++) {

                        // Grab the 'key' which will be the content-type
                        responseToSend.addDataItem(response.body.aggregations['file-extension-list'].buckets[i]['key']);
                    }

                    // Clear the fields
                    responseToSend.clearDataFields();

                    // Send it
                    res.status(200).send(responseToSend);

                } else {
                    // Nothing was found, return a 404
                    var errorToReturn = new Error();
                    errorToReturn.setCode(404);
                    errorToReturn.setMessage("No file extensions found");

                    // Send it
                    res.status(404).send(errorToReturn);
                }
            }
        });
    } else {
        // No repo specified
        var errorToReturn = new Error();
        errorToReturn.setCode(400);
        errorToReturn.setMessage("No repo specified");

        // Send it
        res.status(400).send(errorToReturn);
    }
};

// Grab the logging library and create a logger
var winston = require('winston');
const logger = winston.createLogger({
    level: 'debug',
    transports: [
        new winston.transports.Console()
    ]
});

// Grab the connection to Elasticsearch
var client = require('./connection');

// And the Response and Error defintions too
var Response = require('./response');
var Error = require('./error');
