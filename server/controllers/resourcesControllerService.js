'use strict';

// This method takes a look at the incoming parameters and searches for Resources based on those parameters
module.exports.getResources = function getResources(req, res, next) {

    // Make sure the repo was specified
    if (req && req['repo'] && req['repo'].value) {
        // Grab the repo name
        var repo = req['repo'].value;
        logger.debug("Will search in repo " + repo);

        // First set some defaults for itemsPerPage and startIndex
        var itemsPerPage = 25;
        var startIndex = 0;

        // Check to see if they are specified
        if (req.itemsPerPage && req.itemsPerPage.value) {
            itemsPerPage = req.itemsPerPage.value;
        }
        if (req.startIndex && req.startIndex.value) {
            startIndex = req.startIndex.value;
        }

        // Calculate the start indexes for next and previous
        var nextStartIndex = startIndex + itemsPerPage;
        var previousStartIndex = startIndex - itemsPerPage;
        if (previousStartIndex < 0) previousStartIndex = 0;

        // Build self, next and previous links
        var selfLink = process.env.DC_API_BASE_URL + '/resources?&startIndex=' + startIndex +
            '&itemsPerPage=' + itemsPerPage;
        var nextLink = process.env.DC_API_BASE_URL + '/resources?startIndex=' + nextStartIndex +
            '&itemsPerPage=' + itemsPerPage;
        var previousLink = process.env.DC_API_BASE_URL + '/resources?startIndex=' + previousStartIndex +
            '&itemsPerPage=' + itemsPerPage;

        // Now loop through the properties of the incoming request and tack them on to the base URL
        for (var key in req) {
            if (req[key] && req[key].value && (key != 'startIndex') && key != 'itemsPerPage') {
                selfLink += '&' + key + "=" + encodeURIComponent(req[key].value);
                nextLink += '&' + key + "=" + encodeURIComponent(req[key].value);
                previousLink += '&' + key + "=" + encodeURIComponent(req[key].value);
            }
        }

        // One last check, if startIndex = 0, previousLink should actually be null
        if (startIndex == 0) previousLink = '';

        // Set some default fields to return and then check to see if the caller specified what fields they want returned
        var fieldsToReturn = ["uuid", "file", "locators", "path"];
        if (req['fieldsToReturn'] && req['fieldsToReturn'].value) {
            // Split it into an array using commas
            fieldsToReturn = req['fieldsToReturn'].value.split(',');

            // Iterate over to remove any starting or ending spaces
            for (var i = 0; i < fieldsToReturn.length; i++) {
                fieldsToReturn[i] =
                    fieldsToReturn[i].replace(/^\s\s*/, '').replace(/\s\s*$/, '')
            }

            // Makes sure UUID is in the list as it's required
            if (fieldsToReturn.indexOf('uuid') < 0) {
                fieldsToReturn.push('uuid');
            }
        }

        // *************************************************** //
        // Extract all query params and search clauses
        // *************************************************** //

        // Create a content clause if there is a content parameter
        var contentClause = null;
        if (req.content && req.content.value) {
            contentClause = {
                "match": {
                    "content": {
                        "query": req.content.value
                    }
                }
            };

            // Now there can be two ways to structure this depending on if the caller wants the terms OR'd or AND'd
            contentClause['match']['content']['operator'] = "or";
            if (req.contentAnd) {
                if (req.contentAnd.originalValue === 'true' ||
                    req.contentAnd.originalValue === 'True' ||
                    req.contentAnd.originalValue === 'TRUE') {
                    contentClause['match']['content']['operator'] = "and";
                }
            }
        }

        // If hasData flag is specified, build the clause
        var hasDataClause = null;
        if (req.hasData) {
            if (req.hasData.originalValue === 'true' ||
                req.hasData.originalValue === 'True' ||
                req.hasData.originalValue === 'TRUE') {
                hasDataClause = {
                    "exists": {
                        "field": "data"
                    }
                }
            }
        }

        // If a variable name was specified, create a clause for it
        var variableShortNameClause = null;
        if (req.variableShortName && req.variableShortName.value) {
            variableShortNameClause = {
                "term": {
                    "data.variables.short_name": req.variableShortName.value
                }
            }
        }

        // Create a clause for file content type (filter clause)
        var fileContentTypeClause = null;
        if (req.fileContentType && req.fileContentType.value) {
            fileContentTypeClause = {
                "term": {
                    "file.content_type": req.fileContentType.value
                }
            }
        }

        // Create a clause for file extension if specified
        var fileExtensionClause = null;
        if (req.fileExtension && req.fileExtension.value) {
            fileExtensionClause = {
                "term": {
                    "file.extension": req.fileExtension.value
                }
            }
        }

        // Look to see if we need a range query for created date
        var createdDateClause = null;
        // TODO kgomes: add some exception handling around date parsing
        if (req.fileCreatedDateStart && req.fileCreatedDateStart.value) {
            // Create a date object
            var dateStart = new Date(req.fileCreatedDateStart.value);
            createdDateClause = {
                "range": {
                    "file.created": {
                        "gte": dateStart.toISOString()
                    }
                }

            }
        }
        if (req.fileCreatedDateEnd && req.fileCreatedDateEnd.value) {
            // Create a date object
            var dateEnd = new Date(req.fileCreatedDateEnd.value);
            if (!createdDateClause) {
                createdDateClause = {
                    "range": {
                        "file.created": {
                            "lte": dateEnd.toISOString()
                        }
                    }
                }
            } else {
                createdDateClause['range']['file.created']['lte'] = dateEnd.toISOString();
            }
        }

        // Now the same, but for modified date
        var modifiedDateClause = null;
        // TODO kgomes: add some exception handling around date parsing
        if (req.fileModifiedDateStart && req.fileModifiedDateStart.value) {
            // Create a date object
            var dateStart = new Date(req.fileModifiedDateStart.value);
            modifiedDateClause = {
                "range": {
                    "file.last_modified": {
                        "gte": dateStart.toISOString()
                    }
                }

            }
        }
        if (req.fileModifiedDateEnd && req.fileModifiedDateEnd.value) {
            // Create a date object
            var dateEnd = new Date(req.fileModifiedDateEnd.value);
            if (!modifiedDateClause) {
                modifiedDateClause = {
                    "range": {
                        "file.last_modified": {
                            "lte": dateEnd.toISOString()
                        }
                    }
                }
            } else {
                modifiedDateClause['range']['file.last_modified']['lte'] = dateEnd.toISOString();
            }
        }

        // Look for file name parameter and assume it's to be a wildcard search
        // TODO kgomes: make this case insensitive
        var fileNameClause = null;
        if (req.fileName && req.fileName.value) {
            fileNameClause = {
                "wildcard": {
                    "file.file_name": "*" + req.fileName.value + "*"
                }
            }
        }

        // Create a min max file size range clause
        // TODO kgomes: add some exception handling for number parsing from strings
        var fileSizeClause = null;
        if (req.fileSizeMinimum && req.fileSizeMinimum.value) {
            fileSizeClause = {
                "range": {
                    "file.file_size": {
                        "gte": Number(req.fileSizeMinimum.value)
                    }
                }
            }
        }
        if (req.fileSizeMaximum && req.fileSizeMaximum.value) {
            // Make sure the clause exists
            if (!fileSizeClause) {
                fileSizeClause = {
                    "range": {
                        "file.file_size": {
                            "gte": Number(req.fileSizeMinimum.value)
                        }
                    }
                }
            } else {
                fileSizeClause['range']['file.file_size']['lte'] = Number(req.fileSizeMaximum.value);
            }
        }

        // Start with a match all query
        var queryClause = {
            "match_all": {}
        };

        // Now if params were sent in, create query
        if (contentClause || hasDataClause || variableShortNameClause || fileContentTypeClause || fileExtensionClause ||
            createdDateClause || modifiedDateClause || fileNameClause || fileSizeClause) {
            // Create root query clause
            queryClause = {
                "bool": {}
            };

            // Add the content query if present
            if (contentClause) {
                // First create the must clause if not there
                if (!queryClause['bool']['must']) {
                    queryClause['bool']['must'] = []
                }
                // Now add the contentClause
                queryClause['bool']['must'].push(contentClause);
            }

            // Check to see if data is required on the resource
            if (hasDataClause) {
                // Check for filter section first
                if (!queryClause['bool']['filter']) {
                    queryClause['bool']['filter'] = [];
                }
                // Now add the hasData filter
                queryClause['bool']['filter'].push(hasDataClause);
            }

            // If there is a variable name filter, add that
            if (variableShortNameClause) {
                // Check for filter section first
                if (!queryClause['bool']['filter']) {
                    queryClause['bool']['filter'] = [];
                }
                // Now add the variable name filter
                queryClause['bool']['filter'].push(variableShortNameClause);
            }

            // If there is a file content type filter, add that
            if (fileContentTypeClause) {
                // Check for filter section first
                if (!queryClause['bool']['filter']) {
                    queryClause['bool']['filter'] = [];
                }
                // Now add the clause
                queryClause['bool']['filter'].push(fileContentTypeClause);
            }

            // If there is a file extension filter, add that
            if (fileExtensionClause) {
                // Check for filter section first
                if (!queryClause['bool']['filter']) {
                    queryClause['bool']['filter'] = [];
                }
                // Now add the clause
                queryClause['bool']['filter'].push(fileExtensionClause);
            }

            // If there is a created date clause, add that
            if (createdDateClause) {
                // Check for filter section first
                if (!queryClause['bool']['filter']) {
                    queryClause['bool']['filter'] = [];
                }
                // Now add the clause
                queryClause['bool']['filter'].push(createdDateClause);
            }

            // If there is a modified date clause, add that
            if (modifiedDateClause) {
                // Check for filter section first
                if (!queryClause['bool']['filter']) {
                    queryClause['bool']['filter'] = [];
                }
                // Now add the clause
                queryClause['bool']['filter'].push(modifiedDateClause);
            }

            // Add the file name query if present
            if (fileNameClause) {
                // First create the must clause if not there
                if (!queryClause['bool']['must']) {
                    queryClause['bool']['must'] = []
                }
                // Now add the contentClause
                queryClause['bool']['must'].push(fileNameClause);
            }

            // If there is a file size clause, add that
            if (fileSizeClause) {
                // Check for filter section first
                if (!queryClause['bool']['filter']) {
                    queryClause['bool']['filter'] = [];
                }
                // Now add the clause
                queryClause['bool']['filter'].push(fileSizeClause);
            }

        }

        // This is where we start constructing the queries to handle resource searching.
        client.search({
            index: repo,
            type: 'resource',
            body: {
                from: startIndex,
                size: itemsPerPage,
                _source: fieldsToReturn,
                query: queryClause,
            }
        }, function (error, response, status) {
            // Check for any error
            if (error) {
                // This is some kind of server side error that was not expected
                logger.error("Unknown error trying to find Resource");
                logger.error(error.message);

                // Respond with an error
                var errorToReturn = new Error();
                errorToReturn.setCode(500);
                errorToReturn.setMessage("There was an unexpected server error: " + error.message);

                // Send the error
                res.status(500).send(errorToReturn);
            } else {
                // First check to see if anything was found at all
                if (response && response.body && response.body.hits && response.body.hits.hits &&
                    response.body.hits.hits.length > 0) {

                    // Create a response object
                    var responseToSend = new Response();
                    responseToSend.setCode(200);
                    responseToSend.setAPIVersion(process.env.DC_API_VERSION);
                    responseToSend.setDataKind('resources');
                    responseToSend.setDataItemsPerPage(itemsPerPage);
                    responseToSend.setDataStartIndex(startIndex);
                    responseToSend.setDataSelfLink(selfLink);
                    if (response.body.hits.hits.length < itemsPerPage) {
                        responseToSend.setDataNextLink('');
                    } else {
                        responseToSend.setDataNextLink(nextLink);
                    }
                    responseToSend.setDataPreviousLink(previousLink);

                    // Iterate over the results
                    for (var i = 0; i < response.body.hits.hits.length; i++) {

                        // Let's grab the first hit which should be the only object
                        var resourceHit = response.body.hits.hits[i];

                        // Create a the Resource to return
                        var resourceToReturn = new Resource();
                        resourceToReturn.setFromSource(resourceHit['_source']);
                        // Set the repo
                        resourceToReturn.setRepo(repo);
                        responseToSend.addDataItem(resourceToReturn);
                    }

                    // Send it
                    res.status(200).send(responseToSend);
                } else {
                    // Nothing was found, return a 404
                    var errorToReturn = new Error();
                    errorToReturn.setCode(404);
                    errorToReturn.setMessage("No Resources found");

                    // Send it
                    res.status(404).send(errorToReturn);
                }
            }
        });
    } else {
        // No repo specified
        var errorToReturn = new Error();
        errorToReturn.setCode(400);
        errorToReturn.setMessage("No repo specified");

        // Send it
        res.status(400).send(errorToReturn);
    }
};

// Grab the logging library and create a logger
var winston = require('winston');
const logger = winston.createLogger({
    level: 'debug',
    transports: [
        new winston.transports.Console()
    ]
});

// Grab the connection to Elasticsearch
var client = require('./connection');

// Grab the Resource definition
var Resource = require('./resource');

// And the Response and Error defintions too
var Response = require('./response');
var Error = require('./error');
