'use strict'

// Import libraries
var fs = require('fs')
var http = require('http')
var path = require('path')
var cors = require('cors')
var express = require('express')
var bodyParser = require('body-parser')
var oasTools = require('oas-tools')
var jsyaml = require('js-yaml')

// Create the Express server
var app = express()

// Enable CORS
app.use(cors())

// Add the body parser to the Express server
app.use(bodyParser.json({
  strict: false
}))

// Grab the server port from the environment or use default
var serverPort = process.env.DC_API_SERVER_PORT || 8080

// Grab the URL prefix for the Swagger docs from the env or use empty string
var swaggerDocsPrefix = process.env.DC_API_DOCS_PREFIX || ''

// Read in the API spec
var spec = fs.readFileSync(path.join(__dirname, '/api/oas-doc.yaml'), 'utf8')

// Load it as YAML
var oasDoc = jsyaml.safeLoad(spec)

// Create the configuration object for the OAS API
var options_object = {
  controllers: path.join(__dirname, './controllers'),
  loglevel: 'info',
  strict: false,
  router: true,
  validator: true,
  docs: {
    apiDocs: '/api-docs',
    apiDocsPrefix: swaggerDocsPrefix,
    swaggerUi: '/docs',
    swaggerUiPrefix: swaggerDocsPrefix
  }
}

// Configure the OAS tools
oasTools.configure(options_object)

// Now initialize the OAS tools
oasTools.initialize(oasDoc, app, function () {

  // And create the HTTP server
  http.createServer(app).listen(serverPort, function () {
    console.log('App running at http://localhost:' + serverPort)
    console.log('________________________________________________________________')
    if (options_object.docs !== false) {
      console.log('API docs (Swagger UI) available on http://localhost:' + serverPort + swaggerDocsPrefix + '/docs')
      console.log('________________________________________________________________')
    }
  })
})
