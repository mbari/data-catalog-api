# Start with the NodeJS image
FROM node:8

# Add the application code
ADD server /home/node/server

# Define the working directory
WORKDIR /home/node/server

# And define the command
CMD [ "node","index.js" ]