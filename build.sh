#!/bin/bash

# Some clean up first
rm -rf client
mkdir client
rm -rf server/generated
mkdir server/generated
rm -f server/controllers/*Controller.js

# Since the openapi-generator does not support generation of v3 for NodeJS, use OAS tools instead
# (https://www.npmjs.com/package/oas-generator)
cd server
oas-generator ../openapi.yaml -n generated

# Copy any generated files that are needed but missing
if [ ! -f ./index.js ]; then
    cp ./generated/index.js ./index.js
fi
if [ ! -f ./package.json ]; then
    cp ./generated/package.json ./package.json
fi
if [ ! -d ./api ]; then
    mkdir ./api
fi
cp -R ./generated/api/* ./api
if [ ! -d ./controllers ]; then
    mkdir ./controllers
fi
cp ./generated/controllers/*Controller.js ./controllers

# Install node modules if needed
if [ ! -d ./node_modules ]; then
    npm install
fi

# Change to the base directory to create client libraries
cd ..

# Generate client using openapi-generator (https://openapi-generator.tech/)
openapi-generator generate -i openapi.yaml -g java --additional-properties=modelPackage=org.mbari.data.catalog.client.model,apiPackage=org.mbari.data.catalog.client.api,invokerPackage=org.mbari.data.catalog.client,groupId=org.mbari.data.catalog,artifactId=data-catalog-java-client,artifactVersion=0.0.7,artifactUrl=https://bintray.com/beta/#/org-mbari/maven/data-catalog-java-client,artifactDescription=,developerName=KevinGomes,developerEmail=kgomes@mbari.org,developerOrganization=MBARI,developerOrganizationUrl=https://www.mbari.org,licenseName=MIT,dateLibrary=java8,java8=true,scmConnection=scm:git:https://bitbucket.org/mbari/data-catalog-api/,scmDeveloperConnection=scm:git:https://bitbucket.org/mbari/data-catalog-api/,scmUrl=https://bitbucket.org/mbari/data-catalog-api/ -o ./client/java
openapi-generator generate -i openapi.yaml -g javascript --additional-properties=projectName=@mbari/data-catalog-javascript-client -o ./client/javascript
openapi-generator generate -i openapi.yaml -g python -o ./client/python

# Now build the java client jar (skipping JavaDoc as it doesn't always work)
cd client/java
mvn -Dmaven.test.skip=true -Dmaven.javadoc.skip=true clean package
